<?php

namespace Wheelmaker\Graphql;

class Head
{
    protected $type;
    protected $filters = null;
    protected $string;

    public function __construct($type)
    {
        $this->type = $type;
    }

    protected function filtersString()
    {
        foreach ($this->type->data()->filters as $key => $value) {
            if (!is_int($value)) {
                $value = "\"$value\"";
            }
            if ($this->string) {
                $this->string .= ', ';
            }
            $this->string .= "$key: $value";
        }

        if ($this->string) {
            $this->string = "($this->string)";
        }
        return $this->string;
    }

    public function __toString()
    {
        return $this->type->data()->name . $this->filtersString();
    }
}
