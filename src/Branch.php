<?php 

namespace Wheelmaker\Graphql;

class Branch
{
    protected $data;
    protected $bodyAddOns = []; 

    public function __construct($branchData)
    {
      $this->data = $branchData;
      
      foreach($branchData->branches as $subBranchData) {
        $subBranch = new Branch($subBranchData);
        $this->addTobody($subBranch);
      }
      foreach($branchData->edges as $edgeData) {
        $edge = new Edge($edgeData);
        $this->addToBody($edge);
      }
    }
    public function data()
    {
      return $this->data;
    }
    public function __toString()
    {
        $head = new Head($this);
        $body = $this->attributesString();
        $addOns = implode("\n", $this->bodyAddOns);
        $body = "$body\n$addOns";
        $string = Helper::wrap($head, $body);
        return $string;
    }
    public function addTobody($string)
    {
      $this->bodyAddOns[] = $string;
    }

    protected function attributesString()
    {
        return  implode("\n", $this->data->attributes);
    }
}
