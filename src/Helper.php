<?php 

namespace Wheelmaker\Graphql;

class Helper
{
    const indent = '  ';
    const debug = true;  // greater output readability

    public static function addNextPagesResult(&$branch, $nextPages)
    {
        $nextPages = get_object_vars($nextPages);
        foreach ($nextPages as $name => $page) {
            $branch->$name->edges = array_merge($branch->$name->edges, $page->edges);
            $branch->$name->pageInfo = $page->pageInfo;
        }
    }
    /**
     * getEdges
     *
     * @param mixed $branch results branch returned from api
     * @return array of sub edges from the results branch
     *
     * Can be used to determine if additional calls are required
     * to fully populate the sub edges of a return branch
     */
    public static function getEdges($branch)
    {
        $branch = get_object_vars($branch);
        $edges = [];
        foreach ($branch as $k => $v) {
            if (isset($v->edges)) {
                $edges[] = $k;
            }
        }

        return $edges;
    }
    /**
     * hasAnySubNextPages
     *
     * @param \stdClass $branch result branch from api
     * @return boolean true if any sub edges of the result $branch
     * has a next page
     */
    public static function hasAnySubNextPages($branch)
    {
        $branch = get_object_vars($branch);
        foreach ($branch as $sub) {
            if (isset($sub->pageInfo) && $sub->pageInfo->hasNextPage) {
                return true;
            }
        }

        return false;
    }
    /**
     * indentLines
     *
     * @param string $string string with return characters
     * @return string same string but indented with self::indent
     * if debug is set to true.  This is used to enhance query string
     * readability by indenting everything wrapped in curly brackets
     */
    public static function indentLines(string $string)
    {
        if (self::debug) {
            $strings = explode("\n", $string);

            return rtrim(self::indent . implode("\n" . self::indent, $strings));
        }

        return $string;
    }
    /**
     * subEdgesNextData
     *
     * @param array $data used to make inital api call
     * @param object $branch results with paginated sub edges
     * @param mixed $id for branch with sub edges
     * @return array data to make next api call for any next pages
     * neccessary to fill out sub data on $branch
     */
    public static function subEdgesNextData($data, $branch, $id)
    {
        $nextData = [
          'name' => str_singular($data['name']),
          'filters' => [
            'id' => $id
          ]
        ];

        $branch = get_object_vars($branch);
        foreach ($branch as $subName => $sub) {
            if (isset($sub->pageInfo) && $sub->pageInfo->hasNextPage) {
                $edges[$subName] = $data['edges'][$subName];
                $subEdges = $sub->edges;
                $last = end($subEdges);
                // $last = $sub->edges[end(array_keys($sub->edges))];
                $edges[$subName]['filters']['after'] = $last->cursor;
            }
        }
        $nextData['edges'] = $edges;

        return $nextData;
    }
    /**
     * wrap
     *
     * @param string $name outside the curly brackets
     * @param string $content wrapped inside the curly brackets
     * @return string with name first and then content wrapped inside
     * curly brackets
     */
    public static function wrap(string $name, string $content)
    {
        $content = self::indentLines($content);

        return sprintf("%s {\n%s\n}", $name, $content);
    }
}
