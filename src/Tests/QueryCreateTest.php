<?php

namespace Wheelmaker\Graphql\Tests;

use Wheelmaker\Graphql\BranchData;
use PHPUnit\Framework\TestCase;
use Wheelmaker\Graphql\Query;

class QueryCreateTest extends TestCase
{

  protected $simpleBranchData = [
    'name' => 'testBranch',
    'attributes' => [
      'id',
      'name',
      'url'
    ]
  ];
  protected $branchWithEdgeData = [
    'name' => 'testBranch',
    'attributes' => [
      'id',
      'name',
      'url'
    ],
    'edges' => [
      [
        'name' => 'photos',
        'attributes' => [
          'id',
          'name',
          'url'
        ]
      ]
    ]
  ];
  protected $branchWithSubsBranchesData = [
    'name' => 'testBranch',
    'attributes' => [
      'id',
      'name',
      'url'
    ],
    'branches' => [
      [
        'name' => 'subOne',
        'attributes' => [
          'id',
          'title',
          'description'
        ]
      ],
      [
        'name' => 'subTwo',
        'attributes' => [
          'key',
          'image',
          'color'
        ]
      ]
    ]
  ];
  protected $expectedBranchWithSubBranches = 
"{
  testBranch {
    id
    name
    url
    subOne {
      id
      title
      description
    }
    subTwo {
      key
      image
      color
    }
  }
}";
  protected $expectedSimpleBranch = 
"{
  testBranch {
    id
    name
    url
  }
}";
  protected $expectedBranchWithEdge = 
"{
  testBranch {
    id
    name
    url
    photos(first: 10) {
      edges {
        node {
          id
          name
          url
        }
        cursor
      }
      pageInfo {
        hasNextPage
      }
    }
  }
}";

  function test_create_simple_branch()
  { 
    $branchObject = Query::create()->addBranch($this->simpleBranchData);
    $queryString = trim($branchObject->__toString());
    $this->assertEquals($queryString, $this->expectedSimpleBranch);
  }

  function test_create_branch_with_sub_branches()
  {
    $branchObject = Query::create()->addBranch($this->branchWithSubsBranchesData);
    $queryString = trim($branchObject->__toString());
    $this->assertEquals($queryString, $this->expectedBranchWithSubBranches);
  }
  function test_create_branch_with_edge()
  {
    $branchObject = Query::create()->addBranch($this->branchWithEdgeData);
    $queryString = trim($branchObject->__toString());
    $this->assertEquals($queryString, $this->expectedBranchWithEdge);
  }
}