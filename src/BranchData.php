<?php

namespace Wheelmaker\Graphql;

class BranchData
{
    protected $rawData = [
      'filters' => [],
      'branches' => [],
      'edges' => [],
    ];
    protected $edges = [];
    protected $branches = [];
    protected $filters = [];
    protected $attributes;
    protected $name;

    public function __construct(array $data)
    {
        $this->name = $data['name'];
        if(isset($data['attributes'])) {
          $this->attributes = $data['attributes'];
        } else {
          $this->attributes = [];
        }
        $this->rawData = array_merge_recursive($this->rawData, $data);
        foreach ($this->rawData['branches'] as $branchData) {
            $branchDataObject = new BranchData($branchData);
            $this->branches[] = $branchDataObject;
        }
        foreach ($this->rawData['edges'] as $edgeData) {
            $edgeDataObject = new EdgeData($edgeData);
            $this->edges[] = $edgeDataObject;
        }
        $this->filters += $this->rawData['filters'];
    }
    public function __get($name)
    {
        return $this->$name;
    }

    public function addBranch($data)
    {
        $this->branches[] = new BranchData($data);
    }
    public function addEdge($data)
    {
        $this->edges[] = new EdgeData($data);
    }
    public function filters($filters)
    {
        $this->filters += $filters;
    }
}
