<?php 

namespace Wheelmaker\Graphql;

class Edge extends Branch
{
    public function __construct($edgeData)
    {
      parent::__construct($edgeData);
      if(isset($edgeData->pageSize)) {
        $this->pageSize = $edgeData->pageSize;
      }
    }
    public function __toString()
    {
        $this->hydrateFilters();
        $head = new Head($this);
        $body = $this->attributesString();
        $addOns = implode("\n", $this->bodyAddOns);
        $body = "$body\n$addOns";
        $body = Helper::wrap('node', $body);
        $body .= "\ncursor\n";
        $body = Helper::wrap('edges', $body);
        $this->appendPageInfo($body);
        $string = Helper::wrap($head, $body);
        
        return $string;
    }

    protected function appendPageInfo(&$string)
    {
      $string .= "\n".Helper::wrap('pageInfo', 'hasNextPage');
    }
    protected function hydrateFilters()
    {
      $this->data->filters(['first' => $this->data->pageSize]);
    }
    
}
