<?php

namespace Wheelmaker\Graphql;

class Query
{
    protected $string;

    public function __toString()
    {
        $string = Helper::indentLines($this->string);
        $string = "{\n$string\n}";

        return $string;
    }

    public static function create() 
    {
      return new self();
    }
    public static function createWithBranch($data)
    {
      $query = new self();
      $query->addBranch($data);

      return $query;
    }
    public static function createWithEdge($data)
    {
      $query = new self();
      $query->addEdge($data);

      return $query;
    }
    
    public function addBranch($data)
    {
        $branchDataObject = new BranchData($data);
        $branch = new Branch($branchDataObject);
        $this->string .= "$branch\n";
        return $this;
    }
    public function addEdge($data)
    {
      $edgeDataObject = new EdgeData($data);
      $edge = new Edge($edgeDataObject);
      $this->string .= "$edge\n";
      return $this;
    }
}
