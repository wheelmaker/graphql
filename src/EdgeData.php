<?php

namespace Wheelmaker\Graphql;

class EdgeData extends BranchData{

  protected $pageSize = 10;

  public function __construct($data)
  {
    parent::__construct($data);
    if (isset($this->rawData['pageSize'])) {
        $this->pageSize($this->rawData['pageSize']);
    }
  }

  public function pageSize(int $pageSize)
  {
    $this->pageSize = $pageSize;
  }
}